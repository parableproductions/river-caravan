<?php
// Template Name: Landing
the_post();
get_header(); 

$banner_title = get_field('banner_title');
$banner_sub_title = get_field('banner_sub_title');

$homebrochure = get_field('add_brochure_home');
$brochureurl = $homebrochure['url'];

$home_brochure_background = get_field('home_brochure_background');
$homebrochurebackground = $home_brochure_background['sizes']['large'];

$home_brochure_display = get_field('home_brochure_display');
$homebrochuredisplay = $home_brochure_display['sizes']['large'];

$latest_news_description = get_field('latest_news_description');

?>

<section class="landing" id="landing">
    <div class="wrapper">
        <div class="landing__wrap">
        
            <?php if( have_rows('landing_banner_image') ): ?>
            
                <?php while( have_rows('landing_banner_image') ): the_row(); 
                    $landing_banner_slider = get_sub_field('landing_banner_slider');
                    $landingbannerslider = $landing_banner_slider['sizes']['large'];
                ?>
                
                    <div class="landing__wrap--slider" style="background: url(<?php echo $landingbannerslider ?>) no-repeat center/cover;"></div>
            
                <?php endwhile; ?>
            <?php endif; ?>
        </div>

        
        <div class="body-content">
             <h1><?php echo $banner_title;?></h1>
             <h3><?php echo $banner_sub_title;?></h3>

             <!-- <div class="body-content__contact">
                <h5><a href="https://goo.gl/maps/2WN9TjUffFRU1uZi7" target="_blank">23 Fleet Street Somerton Vic 3062</a><br/>
                    <a href="tel:0393085003">03 9308 5003</a><br/>
                    <a href="mailto:info@rivercaravans.com.au">info@rivercaravans.com.au</a><br/> <a href="mailto:sales@rivercaravans.com.au">sales@rivercaravans.com.au</a>
                </h5>
             </div> -->
        </div>
        <div class="overlay-wrap"></div>
    </div>
</section>

<section class="brochure" id="brochure" style="background: url(<?php echo $homebrochurebackground; ?>) no-repeat center/cover;">
    
    <?php if($homebrochuredisplay):?>
        <img class="brochuredisplay" src="<?php echo $homebrochuredisplay;?>" alt="">
    <?php endif;?>


    <div class="container">
        <div class="brochure__wrap">
            <div class="title">
                <h1>Brochure</h1>
                <p>View our full 2021 Range Brochure</p>
            </div>
            <div class="brochure-btn-wrap">
                <?php if( get_field('add_brochure_home') ): ?>
                    <a class="btn" href="<?php echo $brochureurl;?>" download >Download Brochure</a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>



<section class="latest-news" id="latest-news">
    <div class="container">
        <div class="latest-news__wrap">
            <div class="title">
                <h1>Latest News & Events</h1>
            </div>
            
            <div class="latest-news__wrap-description">
                <p><?php echo $latest_news_description;?></p>
            </div>

            <div class="news__post">
                <?php echo do_shortcode('[pt_view id=f50c034ac0]'); ?>
            </div>

            <!-- <div class="view_btn">
                <a class="btn" href="/news">View More</a>
            </div> -->
        </div>
    </div>
</section>



<section class="partners" id="partners">
    <div class="container">
        <h1>Our  Dealers</h1>
        
        <div class="customer-logos slider">           
            <?php the_post();?>
            <?php if(have_rows('our_partner_slider')): ?>
                <?php while( have_rows('our_partner_slider') ): the_row(); 
                    $our_partner_logo = get_sub_field('our_partner_logo');
                    $ourpartnerlogo = $our_partner_logo['sizes']['large'];
                    $our_partner_website_link = get_sub_field('our_partner_website_link');
                    $ourpartnerwebsitelink = $our_partner_website_link['url']
                ?>
                
                    <div class="slide">
                        <a href="<?php echo $ourpartnerwebsitelink; ?>" target="_blank"><img src="<?php echo $ourpartnerlogo;?>" alt=""></a>
                    </div>
            
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</section>


<?php get_footer(); ?>
