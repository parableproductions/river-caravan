<?php
// Template Name: Contact Us
the_post();
get_header();

$contact_banner_image = get_field('contact_banner_image');
$contactbannerimage = $contact_banner_image['sizes']['large'];

$contactshortcode = do_shortcode('[ninja_form id=1]');
?>

<section class="contact-landing" id="contact-landing" style="background: url(<?php echo $contactbannerimage; ?>) no-repeat center/cover;">
   <div class="body-content">
        <?php if(the_title):?>
            <h1><?php echo the_title(); ?></h1>
        <?php endif; ?>
   </div>
   <div class="overlay-wrap"></div>
</section>

<section class="contact" id="contact">
    <div class="container">
        <div class="contact-wrap">
            <!-- <div class="title" id="title">
                <h1>Get In Touch</h1>
            </div> -->

            <div class="contact-wrap__content">
                <div class="row">
                    <div class="col-md-6">
                        <div class="content">
                            <?php echo $contactshortcode;?>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="content-address">
                            <p><a href="https://goo.gl/maps/HKYVxZWwa3pWQ7PN9" target="_blank"><i class="fal fa-map-marked-alt"></i>30 Fleet Street, Somerton VIC 3062</a></p>

                            <p><a href="tel:0393085003"><i class="fal fa-phone-alt"></i>03 9308 5003</a></p>

                            <p><a href="mailto:info@rivercaravans.com.au"><i class="fal fa-envelope-open-text"></i>info@rivercaravans.com.au</a></p>
    
                            <p><a href="mailto:sales@rivercaravans.com.au"><i class="fal fa-envelope-open-text"></i>sales@rivercaravans.com.au</a></p>
                        </div>

                        <div class="embed-map">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3159.2403872058053!2d144.95903255136065!3d-37.64355217968428!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad64e2aaaa3faa9%3A0x470265d2d77da3c8!2s30%20Fleet%20St%2C%20Somerton%20VIC%203062!5e0!3m2!1sen!2sau!4v1623826584383!5m2!1sen!2sau" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer();?>