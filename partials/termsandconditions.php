<?php
// Template Name: Terms & Conditions
the_post();
get_header();
?>

<section class="privacy-landing" id="privacy-landing" style="background: url(<?php lp_image_dir(); ?>/image09.jpg) no-repeat center/cover;">
   <div class="body-content">
         <h1>Terms & Conditions</h1>
   </div>
   <div class="overlay-wrap"></div>
</section>

<section class="privacy" id="privacy">
    <div class="container">
        <div class="privacy__wrap">
            <?php echo the_content();?>
        </div>
    </div>
</section>


<?php get_footer();?>