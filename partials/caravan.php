
    <div class="card border card-scale">
        <div class="card-body text-center">
            <div class="card-img-top" >
                <?php the_post_thumbnail(); ?>
            </div>

            <div class="card-title p-3 ">
                <h6><?php the_title(); ?></h6>
            </div>

            <div class="caravans-card__btn p-3">
                <a class="btn pt-1 pb-1" href="<?php the_permalink(); ?>">Read More</a>
            </div>
        </div>
    </div>
