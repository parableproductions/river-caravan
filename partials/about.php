<?php
// Template Name: About Us
the_post();
get_header();

$about_banner_image = get_field('about_banner_image');
$aboutbanner = $about_banner_image['sizes']['large'];

$about_description = get_field('about_description');

$about_description_image = get_field('about_description_image');
$aboutdescriptionimage = $about_description_image['sizes']['large'];

$extra_long_description = get_field('extra_long_description');

$our_brand_title = get_field('our_brand_title');




?>

<section class="about-landing" id="about-landing" style="background: url(<?php echo $aboutbanner; ?>) no-repeat center/cover;">
   <div class="body-content">
        <?php if(the_title):?>
            <h1><?php echo the_title(); ?></h1>
        <?php endif; ?>
   </div>
   <div class="overlay-wrap"></div>
</section>

<section class="about" id="about">
    <div class="container">
        <div class="about-wrap">
            <div class="title" id="title">
                <!-- <h1>About Us</h1> -->
            </div>

            <div class="about-wrap__content">
                <div class="row">
                    <div class="col-lg-6 col-md-12">
                        <div class="content pt-3">
                            <?php if($about_description):?>
                                <p><?php echo $about_description;?></p>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-12">
                        <div class="content-image">
                            <?php if($aboutdescriptionimage):?>
                                <img src="<?php echo $aboutdescriptionimage;?>">
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="content">
                            <p><?php echo $extra_long_description;?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="other-brands">
            <div class="container">
                <div class="title">
                    <?php if($our_brand_title):?>
                        <h1><?php echo $our_brand_title; ?></h1>
                    <?php endif; ?>
                </div>
                <div class="brand-logo">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="logo d-flex">
                                <?php if( have_rows('about_other_brands') ): ?>
		
                                    <?php while( have_rows('about_other_brands') ): the_row(); 
                                        $other_brand_image = get_sub_field('other_brand_image');
                                        $otherbrandimage = $other_brand_image['sizes']['large'];

                                        $other_brand_name = get_sub_field('other_brand_name');
                                          
                                        ?>
                                        
                                        <div class="border">
                                        
                                        <a style="padding:5px;" href="#"><img style="height:150px; margin:0;" src="<?php echo $otherbrandimage ?>" /></a>

                                        <h6><?php echo $other_brand_name;?></h6>
                                        </div>

                                        
                                    <?php endwhile; ?>

                                <?php endif; ?>
                            </div>
                        </div>

                        

                        <!-- <div class="col-md-4">
                            <div class="logo">
                                <a href="#"><img src="<?php lp_image_dir(); ?>/placeholder.jpeg" alt=""></a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="logo">
                                <a href="#"><img src="<?php lp_image_dir(); ?>/placeholder.jpeg" alt=""></a>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>