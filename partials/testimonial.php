<?php
// Template Name: Testimonial
the_post();
get_header();
$testimonial_banner_image = get_field('testimonial_banner_image');
$testimonialbanner = $testimonial_banner_image['sizes']['large'];


?>

<section class="testimonial-landing" id="testimonial-landing" style="background: url(<?php echo $testimonialbanner; ?>) no-repeat center/cover;">
   <div class="body-content">
        <?php if(the_title):?>
            <h1><?php echo the_title(); ?></h1>
        <?php endif; ?>
   </div>
   <div class="overlay-wrap"></div>
</section>

<section class="testimonial" id="testimonial">
    <div class="container">
        <div class="testimonial-wrap" id="testimonial-wrap">
            <div class="title" id="title">
                <!-- <h1>Testimonials</h1> -->
            </div>
        </div>
        <div class="wrapper">
            <div class="carousel">
                <?php if( have_rows('add_testimonial') ): ?>
                <?php while( have_rows('add_testimonial') ): the_row(); ?>
                <?php
                  $writetestimonial = get_sub_field('write_testimonial');
                  $authorname = get_sub_field('author_name');
                  ?>

                <div class="slick-container">
                    <h2><em><q><?php echo $writetestimonial; ?></q></em></h2>
                    <h5><?php echo $authorname; ?></h5>
                </div>
                <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>