<?php
// Template Name: Become a Dealer
the_post();
get_header();
?>

<section class="becomeadealer-landing" id="becomeadealer-landing" style="background: url(<?php lp_image_dir(); ?>/image09.jpg) no-repeat center/cover;">
   <div class="body-content">
         <h1>Become a Dealer</h1>
   </div>
   <div class="overlay-wrap"></div>
</section>

<section class="becomedealer" id="becomedealer">
    <div class="container">
        <div class="becomedealer-wrap">
            <div class="title" id="title">
                <h1>Dealership Enquiry Form</h1>
            </div>

            <div class="becomedealer-wrap__content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="content">
                            <?php echo do_shortcode('[ninja_form id=3]');?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<?php get_footer();?>