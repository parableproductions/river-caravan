<?php
// Template Name: News & Events
the_post();
get_header();

$news_banner_image = get_field('news_banner_image');
$newsbanner = $news_banner_image['sizes']['large'];
?>

<section class="news-landing" id="news-landing" style="background: url(<?php echo $newsbanner; ?>) no-repeat center/cover;">
   <div class="body-content">
        <?php if(the_title):?>
            <h1><?php echo the_title(); ?></h1>
         <?php endif; ?>
   </div>
   <div class="overlay-wrap"></div>
</section>

<section class="news" id="news">
    <div class="container">
        <div class="news__wrap">
            <div class="title">
                <!-- <h1>News & Events</h1> -->
            </div>
        </div>

        <div class="news__post">
            <?php echo do_shortcode("[pt_view id=d907e8d0fi]"); ?>
        </div>
    </div>
</section>

<?php get_footer();?>