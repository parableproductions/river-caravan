<?php
// Template Name: Dealer
the_post();
get_header();

$find_dealer_banner_image = get_field('find_dealer_banner_image');
$finddealerbanner = $find_dealer_banner_image['sizes']['large'];

$find_dealer_description = get_field('find_dealer_description');

?>

<section class="dealer-landing" id="dealer-landing" style="background: url(<?php echo $finddealerbanner; ?>) no-repeat center/cover;">
   <div class="body-content">
        <?php if(the_title):?>
            <h1><?php echo the_title(); ?></h1>
        <?php endif; ?>
   </div>
   <div class="overlay-wrap"></div>
</section>

<section class="dealer" id="dealer">
    <div class="container">
        <div class="dealer-wrap">
            <div class="title" id="title">
                <!-- <h1>Find a Dealer</h1> -->
            </div>

            <div class="dealer-wrap__content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="content">
                            <?php if($find_dealer_description):?>
                                <?php echo $find_dealer_description;?>
                            <?php endif;?>
                            
                            <?php the_content();?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php get_footer();?>