<?php 
the_post();
get_header(); 
?>

<!-- <div class="container">
	<?php the_content(); ?>
</div> -->

<section class="news-landing" id="news-landing" style="background: url(<?php lp_image_dir(); ?>/image02.jpg) no-repeat center/cover;">
   <div class="body-content">
         <h1>News & Events</h1>
   </div>
   <div class="overlay-wrap"></div>
</section>

<div class="container">
	<div class="single_wrap">

		<div class="single_wrap__title">
			<h4><?php the_title(); ?></h4>
		</div>

		<div class="single_wrap__image" style="background: url('<?php echo $thumb;?>') no-repeat center/cover;">
			<?php the_post_thumbnail(); ?>
		</div>
		
		<div class="single_wrap__content">
			<?php the_content(); ?>
		</div>

		<div class="single_wrap__btn">
			<a class="btn" href="<?php the_permalink(); ?>/news">back</a>
		</div>
	</div>
</div>

<?php get_footer(); ?>