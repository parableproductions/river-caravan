	<footer class="site-footer">
		<!-- Footer goes here -->
		<div class="site-footer__wrapper">
            <div class="top-footer" id="top-footer">
                <div class="container">
                    <div class="row">
                    <div class="col-md-12">
                            <div class="title">
                                <h5>Subscribe Us About Latest Updates</h5>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-fields">
                                <!-- <input type="text" placeholder="Full Name">
                                <input type="email" placeholder="Email">
                                <input type="text" placeholder="States">
                                <input type="zip" placeholder="ZIP Code">
                                <button type="submit">Submit</button>    -->

                                <?php echo do_shortcode('[ninja_form id=2]'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="middle-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-3 col-md-3">
                            <div class="logo">
                                <a href="<?php echo get_site_url(); ?>" target="_blank"><img src="<?php lp_image_dir(); ?>/river-logo.png"></a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3">
                             <ul>
                                <li><a href="#">RC - Models</a></li>
                                <li><a href="<?php echo get_site_url(); ?>/become-a-dealer/">Become a Dealer</a></li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3">
                             <ul>
                                <li><a href="<?php echo get_site_url(); ?>/about/">About Us</a></li>
                                <li><a href="<?php echo get_site_url(); ?>/contact/">Make an Enquiry</a></li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3">
                            <ul>
                                <li><a href="<?php echo get_site_url(); ?>/news/">News & Events</a></li>
                                <li><a href="<?php echo get_site_url(); ?>/privacy-policy/">Privacy Policy</a></li>
                                <li><a href="<?php echo get_site_url(); ?>/terms-conditions/">Terms & Conditions</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="bottom-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <p>Copyright © <?php echo date("Y"); ?> River Caravans. All rights reserved</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	
	</footer>
</div> <!-- end wrapper -->
<?php wp_footer(); ?>
</body>
</html>