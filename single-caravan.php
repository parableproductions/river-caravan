<?php 
the_post();
get_header(); 

$backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );

$exterior_description = get_field('exterior_description');
$interior_description = get_field('interior_description');
$river_factory_warranty = get_field('river_factory_warranty');

$single_caravan_brochure = get_field('single_caravan_brochure');
$singlecaravanbrochure = $single_caravan_brochure['url'];

?>


<section class="caravan-landing" id="caravan-landing" style="background: url('<?php echo $backgroundImg[0]; ?>') no-repeat center/cover;">
   <div class="body-content">
         <h1><?php the_title(); ?></h1>
   </div>
   <div class="overlay-wrap"></div>
</section>

<section class="caravan" id="caravan">
	
	<div class="container">
		<div class="caravan_wrap">

			<div class="caravan_wrap__title">
				<h4><?php the_title(); ?></h4>
			</div>

			<div class="caravan_wrap__info text-center">
				<p><?php the_content();?></p>
			</div>

		</div>
	</div>

	<div class="container">
		<div class="row btn-border">
			<div class="caravan_btn">
				<a class="btn single-btn" href="#floorplan" onclick="caravansingleproduct('floorplan')">Floor Plan</a>
				<a class="btn single-btn" href="#specifications" onclick="caravansingleproduct('specifications')">Specification</a>
				<a class="btn single-btn" href="<?php echo $singlecaravanbrochure;?>" download >Download Brochure</a>
			</div>
		</div>

		<div class="caravan_details">

			<div class="caravan_details__floorplan singlecaravantab" id="floorplan">
				
				<div class="row">
					<div class="col-md-12">
						<div class="title">
							<h3>Floor Plan</h3>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="floorplan-description text-center">
							<p>*Caravan Layouts and specifications are subject to change. Check with your local dealer for an updated list and plans</p>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-8 floor-size">
						<div class="slideshow">
							<div class="slider-slick">
								<?php if( have_rows('caravan_specification_slider') ): ?>

									<?php while( have_rows('caravan_specification_slider') ): the_row();
										$specification_images = get_sub_field('specification_images');
										$specificationimages = $specification_images['sizes']['large'];
									?>
										<div class="item">
											<img src="<?php echo $specificationimages ?>" alt="">
										</div>
									<?php endwhile;?>
								<?php endif;?>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="floorplan-specs">
							<h4>River Factory Warranty</h4>
							<p><?php echo $river_factory_warranty; ?></p>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="condition">
							<small>*To find out more about River Caravans. Please download the lastest brochure.</small>
						</div>
					</div>
				</div>
			</div>

			<div class="caravan_details__specifications singlecaravantab" id="specifications">

				<div class="row">
					<div class="col-md-12">
						<div class="title">
							<h3>Key Features</h3>
						</div>
					</div>
				</div>

				<div class="specification-type">
					<div class="row">
						<div class="col-md-4">
							<div class="specifications-internal">

								<?php if(have_rows('specifications_internal')):?>
									<ul class="specifications-internal_ul">
										<?php while(have_rows('specifications_internal')): the_row();
										$internal_list = get_sub_field('internal_list');
										?>
										<li class="specifications-internal_li d-flex"><i class="fas fa-star mr-2"></i><p class="text-left"><?php echo $internal_list;?></p></li>&nbsp;
										<?php endwhile;?>
									</ul>
								<?php endif;?>
							</div>
						</div>

						<div class="col-md-4">
							<div class="specifications-appliances">

								<?php if(have_rows('specifications_appliances')):?>
									<ul class="specifications-appliances_ul">
										<?php while(have_rows('specifications_appliances')): the_row();
										$appliances_list = get_sub_field('appliances_list');
										?>
										<li class="specifications-appliances_li d-flex"><i class="fas fa-star mr-2"></i><p class="text-left"><?php echo $appliances_list;?></p></li>&nbsp;
										<?php endwhile;?>
									</ul>
								<?php endif;?>
							</div>
						</div>

						<div class="col-md-4">
							<div class="specifications-plumbing">

								<?php if(have_rows('specifications_plumbing')):?>
									<ul class="specifications-plumbing_ul">
										<?php while(have_rows('specifications_plumbing')): the_row();
										$plumbing_list = get_sub_field('plumbing_list');
										?>
										<li class="specifications-plumbing_li d-flex"><i class="fas fa-star mr-2"></i><p class="text-left"><?php echo $plumbing_list;?></p></li>&nbsp;
										<?php endwhile;?>
									</ul>
								<?php endif;?>
							</div>
						</div>
					</div>
				</div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="condition text-center">
                            <small>*For full specifications and standard features please download the brochure.</small>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</div>

    <div class="container">
        <div class="caravan_slider_exterior-section">
            <div class="caravan_slider_exterior">
                <?php if( have_rows('caravan_exterior') ): ?>
                    <?php while( have_rows('caravan_exterior') ): the_row();
                        $exterior_image = get_sub_field('exterior_image');
                        $exteriorimage = $exterior_image['sizes']['large'];
                        ?>
                        <div><img src="<?php echo $exteriorimage; ?>"/> </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>

        <div class="caravan_slider_interior-section">
            <div class="caravan_slider_interior">
                <?php if( have_rows('caravan_interior') ): ?>
                    <?php while( have_rows('caravan_interior') ): the_row();
                        $caravan_interior = get_sub_field('interior_image');
                        $caravaninterior = $caravan_interior['sizes']['large'];
                        ?>
                        <div><img src="<?php echo $caravaninterior; ?>" /> </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>


<?php get_footer(); ?>