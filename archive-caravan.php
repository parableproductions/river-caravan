<?php get_header(); ?>

<section class="caravan-landing" id="caravan-landing" style="background: url(<?php lp_image_dir(); ?>/image07.jpg) no-repeat center/cover;">
   <div class="body-content">
         <h1>Our Range</h1>
   </div>
   <div class="overlay-wrap"></div>
</section>

<section class="range_description">
	<h4>Explore the Wide Range of River Caravans Collection</h4>
</section>

<div class="container">
	<?php if(have_posts()): ?>
		<div class="row">
			<?php while(have_posts()): the_post(); ?>
			<section class="col-md-3 pt-5 pb-5" id="caravan-range">
					<?php include locate_template('partials/caravan.php'); ?>
			</section>
			<?php endwhile; ?>
            
		</div>

		<div class="pagination">
				<?php
					print paginate_links(array(
						'current'   => max( 1, get_query_var( 'paged' ) ),
						'total'     => $wp_query->max_num_pages,
						'prev_text' => lp_fa('fa fa-angle-left', 'Previous'),
						'next_text' => lp_fa('fa fa-angle-right', 'Next'),
						'type'      => 'list',
						'end_size'  => 3,
						'mid_size'  => 3
					));
				?>
			</div>

	    <?php else: ?>
		<div class="no-posts">
			<p>No posts were found</p>
		</div>
	<?php endif; ?>
</div>

<?php get_footer(); ?>