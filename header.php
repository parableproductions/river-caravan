<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <!--<link rel="profile" href="https://kenwheeler.github.io/slick/slick/slick.js">-->


    <?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php endif; ?>

    <?php wp_head(); ?>

    <script type="text/javascript">
    var _ajaxurl = '<?= admin_url("admin-ajax.php"); ?>';
    var _pageid = '<?= get_the_ID(); ?>';
    var _imagedir = '<?php lp_image_dir(); ?>';
    </script>


    <link rel="apple-touch-icon" sizes="180x180" href="<?php lp_image_dir(); ?>/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php lp_image_dir(); ?>/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php lp_image_dir(); ?>/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php lp_image_dir(); ?>/favicon/site.webmanifest">
</head>

<body <?php body_class(); ?>>

    <?php

// This fixes an issue where wp_nav_menu applied the_title filter which causes WC and plugins to change nav menu labels
print '<!--';
the_title();
print '-->';

?>

<div id="top"></div>
    <div class="wrapper">
        <div class="sticky-btn">
            <div class="sticky-btn__wrap">
                <a target="_blank" href="/our-range"><i class="fal fa-caravan-alt"></i></a>
                <a target="_blank" href="https://www.facebook.com/RIVER-Caravans-105212741699662"><i
                        class="fab fa-facebook-square"></i></a>
                <a target="_blank" href="/contact-us"><i class="fal fa-envelope-open-text"></i></a>
                <a target="_blank" href="https://rivercaravans.com.au/wp-content/uploads/2021/06/RiverCaravans_Brochure_2022.pdf"><i class="fal fa-book-open"></i></a>
            </div>
        </div>
    </div>
</div>

<div class="header">
    <div class="wrapper">
        <header class="site-header">
                <!-- Header goes here -->
            <div class="custom-container">
                <div class="menu-bar">
                    <div class="menu-bar__left">
                        <a href="/"><img class="logo" src="<?php lp_image_dir(); ?>/river-logo4.png" width="150"></a>
                    </div>
                    <div class="menu-bar__right">
                        <div class="mobile-logo">
                            <a href="/"><img class="logo" src="<?php lp_image_dir(); ?>/river-logo4.png" width="200"></a>
                        </div>
                        <?php ubermenu( 'main' , array( 'theme_location' => 'header-menu' ) ); ?>
                    </div>
                </div>
            </div>
        </header>
    </div>
</div>